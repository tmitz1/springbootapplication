drop schema if exists springBoot12;
CREATE
DATABASE IF NOT EXISTS springBoot12;
use
springBoot12;

create table customer
(
    id          int primary key auto_increment,
    first_name  varchar(30),
    last_name   varchar(50),
    city        varchar(50),
    zip_code    char(6),
    member_since date
);

insert into customer(first_name, last_name, city, zip_code, member_since)
values ("Boby", "Doe", "Charlotte", "28205", current_date() - interval 6 day),
       ("Jane", "Smith", "London", "123456", current_date() - interval 3 day),
       ("Rory", "Brown", "Phoenix", "719462", current_date() - interval 1 day);

create table policy
(
    id              int primary key auto_increment,
    policy_number   varchar(30),
    type            varchar(100),
    description     varchar(300),
    monthly_premium double
);

insert into policy(policy_number, type, description, monthly_premium)
values ("356M125498", "Collision coverage", "Helps pay to repair or replace your vehicle if it is damaged or destroyed in an accident, regardless of who is at fault.", 100),
       ("945D312478", "Liability coverage", "Helps pay for the other driver''s expenses if you cause a car accident.", 300),
       ("128A312473", "Medical payment coverage", "Helps pay your or your passengers'' medical expenses if you''re injured in a car accident.", 125),
       ("198W217569", "Roadside coverage", "If your car breaks down and you need roadside assistance, roadside protection can help cover the costs.", 200),
       ("364E215896", "Personal injury protection", "Provides coverage for OTHER THAN bodily injury or property damage", 50),
       ("364U354128", "Personal umbrella policy", "Covers claims in excess of regular homeowners, auto, or watercraft policy coverage.", 80),
       ("952R123934", "Rental reimbursement coverage", "Helps pay your rental car costs while your car is being repaired as a result of a covered claim.", 120);

create table customer_policy
(
    id          int primary key auto_increment,
    policy_id   int not null,
    customer_id int not null,
    joined_date date,
    CONSTRAINT fk_customer FOREIGN KEY (customer_id)
        REFERENCES customer (id)
        on delete cascade
        on update cascade,
    CONSTRAINT fk_policy FOREIGN KEY (policy_id)
        REFERENCES policy (id)
        on delete cascade
        on update cascade
);

insert into customer_policy (customer_id, policy_id, joined_date)
values (1, 1, current_date() - interval 10 day),
       (3, 2, current_date() - interval 2 day),
       (1, 3, current_date() - interval 38 day),
       (2, 4, current_date() - interval 40 day),
       (3, 5, current_date() - interval 30 day),
       (1, 6, current_date() - interval 20 day),
       (2, 5, current_date() - interval 90 day),
       (3, 7, current_date() - interval 100 day),
       (1, 2, current_date() - interval 123 day),
       (3, 3, current_date() - interval 34 day),
       (2, 2, current_date() - interval 15 day);