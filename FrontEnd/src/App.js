import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Button, Box } from '@material-ui/core';
import {createTheme, ThemeProvider} from '@material-ui/core/styles';
import CustomerListPage from './components/CustomerListPage/CustomerListPage';
import PolicyListPage from './components/PolicyListPage/PolicyListPage';
import Breadcrumb from './components/Breadcrumb/Breadcrumb';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import NotFound from './components/NotFound/NotFound';

const theme = createTheme({
  palette: {
    primary: {
      main: "#0075b8"
    }
  }
});

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <ThemeProvider theme={theme}>
          <main>
            <Box m={2}>
              <Breadcrumb />
            </Box>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route exact path="/customer" component={CustomerListPage} />
              <Route exact path="/policy" component={PolicyListPage} />
              <Route path="*" component={NotFound} />
            </Switch>
          </main>
        </ThemeProvider>
        <Footer />
      </Router>
    </div>
  );
};

const HomePage = () => (
  <div className="HomePage">
    <Box m={2}>
      <Button component={Link} to={"/customer"} variant="contained" color="primary">View Customer List</Button>
    </Box>
    <Box m={2}>
      <Button component={Link} to={"/policy"} variant="contained" color="primary">View Policy List</Button>
    </Box>
  </div>
);

export default App;
