import React from 'react';
import './Footer.css';

const Footer = () => (
  <div className="Footer">
    <p><small>AllState Technology Rotational Program 2021</small></p>
  </div>
);

export default Footer;
