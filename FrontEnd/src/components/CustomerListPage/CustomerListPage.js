import React, { useEffect, useState, forwardRef } from 'react';
import './CustomerListPage.css';

import { Link } from 'react-router-dom';
import { Button, Box } from '@material-ui/core';
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
}; // end tableIcons

const CustomerListPage = () => {
  const [data, setData] = useState([]);
  const [isError, setIsError] = useState(false);
  const [errorMessages, setErrorMessages] = useState([]);


  useEffect(() => {
    fetchData().catch(error => {
      setIsError(true);
      setErrorMessages("Cannot load data");
    });
  }, []);

  const fetchData = async () => {
    const rawData = await fetch("http://localhost:8080/customer");
    let jsonData = await rawData.json();
    setData(jsonData);
  };

  const title = "Customer List";

  const columns = [
    { title: 'First Name', field: 'firstName' },
    { title: 'Last Name', field: 'lastName' },
    { title: 'City', field: 'city' },
    { title: 'Zip Code', field: 'zipCode' },
    { title: 'Member Since', field: 'memberSince', type: 'date' },
  ];

  const headerStyle = {
    "color": "black",
    "fontWeight": "bold"
  };
  return (
    <div className="CustomerListPage">
      <MaterialTable
        title={title}
        columns={columns}
        data={data}
        options={{ headerStyle }}
        icons={tableIcons}
        onRowClick={(event, rowData, togglePanel) => togglePanel()}
        detailPanel={rowData => {
          console.log(rowData.heldPolicies)
          return (
            <div className="customer-detail">
              <h3>Held Policies</h3>

              {rowData.heldPolicies.length > 1 ?
                (
                  <ul className="held-policies">
                    {rowData.heldPolicies.map((purchase, index) => {
                      return (
                        <li key={index}>
                          <p>Policy Number: {purchase.policy.policyNumber}<br/>
                          Policy Type: {purchase.policy.policyType}<br/>
                          Monthly Premium: {purchase.policy.monthlyPremium}<br/>
                          Joined Date: {purchase.joinedDate}</p>
                        </li>
                      )
                    })}
                  </ul>
                ) : "No policies purchased yet."}

            </div>
          )
        }}
      />

      <Box m={2}>
        <Button component={Link} to={"/"} variant="outlined" color="primary">Go Back Home</Button>
      </Box>
    </div>
  );
};
export default CustomerListPage;
