import React from 'react';
import './NotFound.css';
import {useLocation, Link} from 'react-router-dom';

const NotFound = () => {
  let location = useLocation();
  return (
    <div className="NotFound">
      <h1>No match for <code>{location.pathname}</code></h1>
      <p>Go back <Link to="/">Home</Link></p>
    </div>)
};

export default NotFound;
