import React, { useEffect, useState, forwardRef } from 'react';
import './PolicyListPage.css';

import { Link } from 'react-router-dom';
import { Button, Box } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const PolicyListPage = () => {
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  }; // end tableIcons

  const [data, setData] = useState([]);
  const [isError, setIsError] = useState(false);
  const [errorMessages, setErrorMessages] = useState([]);

  useEffect(() => {
    fetchData().catch(error => {
      setIsError(true);
      setErrorMessages("Cannot load data");
    });
  }, []); // end useEffect

  const apiUrl = "http://localhost:8080/policy/";

  const fetchData = async () => {
    const rawData = await fetch(apiUrl);
    let jsonData = await rawData.json();

    setData(jsonData);
  }; // end fetchData

  const title = "Policy List";

  const headerStyle = {
    "color": "black",
    "fontWeight": "bold"
  }; // end headerStyle

  const columns = [
    { title: 'id', field: 'id', hidden: true },
    { title: 'Policy Number', field: 'policyNumber' },
    { title: 'Type', field: 'policyType' },
    { title: 'Description', field: 'policyDescription' },
    { title: 'Monthly Premium USD', field: 'monthlyPremium', type: 'numeric' },
  ]; // end columns

  const handleRowUpdate = (newData, oldData, resolve) => {
    let errorList = []
    if (newData.policyNumber == null || newData.policyNumber === "") {
      errorList.push("Please enter policy number")
    }
    if (newData.policyType == null || newData.policyType === "") {
      errorList.push("Please enter policy type")
    }
    if (newData.policyDescription == null || newData.policyDescription === "") {
      errorList.push("Please enter a description")
    }
    if (newData.monthlyPremium == null || newData.monthlyPremium === "") {
      errorList.push("Please enter a monthly premium")
    }

    if (errorList.length < 1) {

      const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newData)
      };

      fetch(apiUrl, requestOptions)
        .then(res => {
          const dataUpdate = [...data];
          const index = oldData.tableData.id;
          dataUpdate[index] = newData;
          setData([...dataUpdate]);
          resolve()
          setIsError(false)
          setErrorMessages([])
        })
        .catch(error => {
          console.log(error);
          setErrorMessages(["Update failed! Server error"]);
          setIsError(true);
          resolve();
        })
    } else {
      setErrorMessages(errorList);
      setIsError(true);
      resolve();
    }
  }

  const handleRowAdd = (newData, resolve) => {
    let errorList = []
    if (newData.policyNumber == null || newData.policyNumber === "") {
      errorList.push("Please enter policy number")
    }
    if (newData.policyType == null || newData.policyType === "") {
      errorList.push("Please enter policy type")
    }
    if (newData.policyDescription == null || newData.policyDescription === "") {
      errorList.push("Please enter a description")
    }
    if (newData.monthlyPremium == null || newData.monthlyPremium === "") {
      errorList.push("Please enter a monthly premium")
    }

    if (errorList.length < 1) { //no error

      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newData)
      };


      fetch(apiUrl, requestOptions)
        .then(res => {
          let dataToAdd = [...data];
          dataToAdd.push(newData);
          setData(dataToAdd);
          resolve();
          setErrorMessages([]);
          setIsError(false);
        })
        .catch(error => {
          console.log(error);
          setErrorMessages(["Cannot add data. Server error!"]);
          setIsError(true);
          resolve();
        })
    } else {
      setErrorMessages(errorList);
      setIsError(true);
      resolve();
    }
  }

  const handleRowDelete = (oldData, resolve) => {

    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' }
    };

    fetch(apiUrl + oldData.id, requestOptions)
      .then(res => {
        const dataDelete = [...data];
        const index = oldData.tableData.id;
        dataDelete.splice(index, 1);
        setData([...dataDelete]);
        resolve();
      })
      .catch(error => {
        setErrorMessages(["Delete failed! Server error"]);
        setIsError(true);
        resolve();
      })
  }

  return (
    <div className="PolicyListPage">

      <div>
        {isError &&
          <Alert severity="error">
            {errorMessages.map((msg, i) => {
              return <div key={i}>{msg}</div>
            })}
          </Alert>
        }
      </div>
      <MaterialTable
        title={title}
        columns={columns}
        data={data}
        icons={tableIcons}
        options={{ headerStyle }}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              handleRowUpdate(newData, oldData, resolve);

            }),
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              handleRowAdd(newData, resolve)
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              handleRowDelete(oldData, resolve)
            }),
        }}
      />

<Box m={2}>
        <Button component={Link} to={"/"} variant="outlined" color="primary">Go Back Home</Button>
      </Box>
    </div>
  ); // end return
}; // end PolicyListPage
export default PolicyListPage;
