import React from 'react';
import logo from '../../logo.jpg';
import './Header.css';
import { Link } from 'react-router-dom';

const Header = () => (
  <div className="Header">
    <Link to="/">
      <img src={logo} className="Header-logo" alt="logo" />
    </Link>
    <nav>
      <ul className="nav-links">
        <Link to="/">
          <li>Home</li>
        </Link>
        <Link to="/customer">
          <li>Customer</li>
        </Link>
        <Link to="/policy">
          <li>Policy</li>
        </Link>
      </ul>
    </nav>
  </div>
);

export default Header;
