import React from 'react';
import './Breadcrumb.css';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import { Link, Route } from 'react-router-dom';

function toTitleCase(str) {
  return str.replace(/\b\w+/g, function (s) {
    return s.charAt(0).toUpperCase() + s.substr(1).toLowerCase()
  })
}

export default function Breadcrumb(props) {
  return (
    <Route>
      {({ location }) => {
        const pathnames = location.pathname.split('/').filter(x => x);
        return (
          <Breadcrumbs separator=">" aria-label="Breadcrumb">
            <Link color="textPrimary" to="/">
              Home
            </Link>
            {pathnames.map((value, index) => {
              const last = index === pathnames.length - 1;
              const to = `/${pathnames.slice(0, index + 1).join('/')}`;

              return last ? (
                <Typography color="textPrimary" key={to}>
                  {toTitleCase(value)}
                </Typography>
              ) : (
                  <Link color="textSecondary" to={to} key={to}>
                    {toTitleCase(value)}
                  </Link>
                );
            })}
          </Breadcrumbs>
        );
      }}
    </Route>
  );
}
