package com.allstate.springboot.model.custom;

import java.time.LocalDate;

public class PolicyJoinedDate {
    private String policyName;
    private LocalDate joinedDate;

    public PolicyJoinedDate(String policyName, LocalDate joinedDate) {
        this.policyName = policyName;
        this.joinedDate = joinedDate;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public LocalDate getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(LocalDate joinedDate) {
        this.joinedDate = joinedDate;
    }

}
