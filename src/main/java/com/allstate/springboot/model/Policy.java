package com.allstate.springboot.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "policy")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Policy implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "policy_number")
    @NonNull
    private String policyNumber;

    @Column(name = "type")
    @NonNull
    private String type;

    @Column(name = "description")
    @NonNull
    private String description;

    @Column(name = "monthly_premium")
    @NonNull
    private double monthlyPremium;

    @OneToMany(mappedBy = "policy")
    private Set<Purchase> customers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyType() {
        return type;
    }

    public void setPolicyType(String type) {
        this.type = type;
    }

    public String getPolicyDescription() {
        return description;
    }

    public void setPolicyDescription(String description) {
        this.description = description;
    }

    public double getMonthlyPremium() {
        return monthlyPremium;
    }

    public void setMonthlyPremium(double monthlyPremium) {
        this.monthlyPremium = monthlyPremium;
    }
}

