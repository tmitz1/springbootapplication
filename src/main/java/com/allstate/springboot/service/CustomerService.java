package com.allstate.springboot.service;

import com.allstate.springboot.model.Customer;
import com.allstate.springboot.model.custom.PolicyJoinedDate;
import com.allstate.springboot.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CustomerService implements ICustomerService {
    private static final int POLICY_DURATION_IN_MONTHS = 6;
    @Autowired
    private CustomerRepo repo;

    @Override
    public List<Customer> findAll() {
        return repo.findAll();
    }

    @Override
    public Customer getCustomerById(int id) {
        return repo.findById(id).get();
    }

    @Override
    public void createCustomer(Customer customer) {
        if (customer.getMemberSince() == null) {
            customer.setMemberSince(LocalDate.now());
        }
        repo.save(customer);
    }

    @Override
    public Customer updateCustomer(Customer updatedCustomer) {
        return repo.findById(updatedCustomer.getId()).map(customer -> {
            customer.setFirstName(updatedCustomer.getFirstName());
            customer.setLastName(updatedCustomer.getLastName());
            customer.setCity(updatedCustomer.getCity());
            customer.setZipCode(updatedCustomer.getZipCode());
            customer.setMemberSince(updatedCustomer.getMemberSince());
            return repo.save(customer);
        }).orElseGet(() -> {
            return repo.save(updatedCustomer);
        });
    }

    @Override
    public void deleteCustomerById(int id) {
        Customer customer = repo.findById(id).get();
        repo.delete(customer);
    }

    @Override
    public void deleteCustomer(Customer customer) {
        repo.delete(customer);
    }

    @Override
    public Map<String, LocalDate> getNextRenewalDatesByCustomerId(int customerId) {
        Map<String, LocalDate> policyRenewalDateMap = new HashMap<>();

        List<PolicyJoinedDate> policyJoinedDates = repo.getPolicyJoinedDates(customerId);

        for (PolicyJoinedDate policyJoinedDate :
                policyJoinedDates) {
            LocalDate renewalDate = policyJoinedDate.getJoinedDate();
            while (renewalDate.isBefore(LocalDate.now())) {
                renewalDate = renewalDate.plusMonths(POLICY_DURATION_IN_MONTHS);
            }
            policyRenewalDateMap.put(policyJoinedDate.getPolicyName(), renewalDate);
        }

        return policyRenewalDateMap;
    }
}