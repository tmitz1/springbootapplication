package com.allstate.springboot.service;

import com.allstate.springboot.model.Customer;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;


public interface ICustomerService {
    Collection<Customer> findAll();

    Customer getCustomerById(int id);

    void createCustomer(Customer customer);

    Customer updateCustomer(Customer updatedCustomer);

    void deleteCustomerById(int id);

    void deleteCustomer(Customer customer);

    Map<String, LocalDate> getNextRenewalDatesByCustomerId(int customerId);
}
