package com.allstate.springboot.service;

import com.allstate.springboot.model.Policy;

import java.util.Collection;

public interface IPolicyService {
    Collection<Policy> findAll();

    Policy getPolicyById(int id);

    void createPolicy(Policy policy);

    Policy updatePolicy(Policy policy);

    void deletePolicyById(int id);

    void deletePolicy(Policy policy);
}
