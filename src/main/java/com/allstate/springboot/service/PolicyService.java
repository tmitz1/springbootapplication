package com.allstate.springboot.service;

import com.allstate.springboot.model.Policy;
import com.allstate.springboot.repo.PolicyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PolicyService implements IPolicyService {

    @Autowired
    private PolicyRepo repo;

    @Override
    public Collection<Policy> findAll() {
        return repo.findAll();
    }

    @Override
    public Policy getPolicyById(int id) {
        return repo.findById(id).get();
    }

    @Override
    public void createPolicy(Policy policy) {
        repo.save(policy);
    }

    @Override
    public Policy updatePolicy(Policy policy) {
        repo.save(policy);
        return getPolicyById(policy.getId());
    }

    @Override
    public void deletePolicyById(int id) {
        repo.deleteById(id);
    }

    @Override
    public void deletePolicy(Policy policy) {
        repo.delete(policy);
    }
}
