package com.allstate.springboot.repo;

import com.allstate.springboot.model.Customer;
import com.allstate.springboot.model.custom.PolicyJoinedDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {
    @Query("select new com.allstate.springboot.model.custom.PolicyJoinedDate(p.type, pc.joinedDate)" +
            " from Customer as c, Policy as p, Purchase as pc" +
            " where p.id = pc.policy.id and c.id = pc.customer.id" +
            " and c.id = :customerId")
    List<PolicyJoinedDate> getPolicyJoinedDates(int customerId);
}