package com.allstate.springboot.controller;

import com.allstate.springboot.model.Customer;
import com.allstate.springboot.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private ICustomerService service;

    @GetMapping
    public Collection<Customer> findAll() {
        return service.findAll();
    }

    @GetMapping("/{customerId}")
    public Customer getCustomerById(@PathVariable("customerId") int id) {
        return service.getCustomerById(id);
    }

    @PostMapping
    public void createCustomer(@RequestBody Customer customer) {
        service.createCustomer(customer);
    }

    @PutMapping
    public Customer updateCustomer(@RequestBody Customer updatedCustomer) {
        return service.updateCustomer(updatedCustomer);
    }

    @DeleteMapping("/{customerId}")
    public void deleteCustomerById(@PathVariable("customerId") int id) {
        service.deleteCustomerById(id);
    }

    @DeleteMapping
    public void deleteCustomer(@RequestBody Customer customer) {
        service.deleteCustomer(customer);
    }

    @GetMapping("/{customerId}/nextRenewalDates")
    public Map<String, LocalDate> getNextRenewalDatesByCustomerId(@PathVariable int customerId) {
        if (service.getCustomerById(customerId) == null) {
            throw new NoSuchElementException("Invalid customer ID");
        }

        return service.getNextRenewalDatesByCustomerId(customerId);
    }
}
