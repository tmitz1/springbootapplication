package com.allstate.springboot.controller;

import com.allstate.springboot.model.Policy;
import com.allstate.springboot.service.IPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@CrossOrigin
@RequestMapping("/policy")
public class PolicyController {
    @Autowired
    private IPolicyService service;

    @GetMapping
    public Collection<Policy> findAll() {
        return service.findAll();
    }

    @GetMapping("/{policyId}")
    public Policy getPolicyById(@PathVariable("policyId") int id) {
        return service.getPolicyById(id);
    }

    @PostMapping
    public void createPolicy(@RequestBody Policy policy) {
        service.createPolicy(policy);
    }

    @PutMapping
    public Policy updatePolicy(@RequestBody Policy policy) {
        return service.updatePolicy(policy);
    }

    @DeleteMapping("/{policyId}")
    public void deletePolicyById(@PathVariable("policyId") int id) {
        service.deletePolicyById(id);
    }

    @DeleteMapping
    public void deletePolicy(@RequestBody Policy policy) {
        service.deletePolicy(policy);
    }
}
